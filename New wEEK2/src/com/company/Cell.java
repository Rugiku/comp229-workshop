package com.company;

import java.awt.*; //Rectangle is accessible
import java.awt.Rectangle;


/**
 * Created by 43326706 on 16/08/2017.
 */


public class Cell extends Rectangle{

    private int x;
    private int y;

    public Cell(int x, int y){
        //this.x = x;
        //this.y = y;
        super(x,y);
        setSize(35, 35);
    }

    public void paint(Graphics g, Boolean highlighted){
        for (int i = 0; i < 20; i++){
            for (int k = 0; k < 20; k++){
                if (highlighted){
                    g.setColor(Color.LIGHT_GRAY);
                    g.fillRect(i * 35, k * 35, 35, 35);
                }
                g.setColor(Color.BLACK);
                g.drawRect(i * 35, k * 35, 35, 35);
            }
        }


        /*if (highlighted){
            g.setColor(Color.LIGHT_GRAY);
            g.fillRect(x, y, 35, 35);
        }
        g.setColor(Color.BLACK);
        g.drawRect(x, y, 35, 35);
        */
    }


    public boolean contains(Point target){
        if (target == null){
            return false;
        }
        if (super.contains(target) != true){
            return false;
        } else {
            return true;
        }

        /*if (target == null) {
            return false;
        } else {
            return target.x > x && target.x < x + 35 && target.y > y && target.y < y + 35;
        }
        */
    }

}
