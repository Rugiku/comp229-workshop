package com.company;
import java.awt.*;
/**
 * Created by 43326706 on 16/08/2017.
 */
public class Grid {

    private Cell[][] grid = new Cell [20][20];

    public Grid (){
        for (int x = 0; x < grid.length; x++){
            for (int y = 0; y < grid[x].length; y++){
                grid[x][y] = new Cell (10 + x * 35, 10 + y * 35);
            }
        }
    }

    public void paint (Graphics g, Point mousePosition){
        for (int x = 0; x < grid.length; x++){
            for (int y = 0; y < grid[x].length; y++){
                grid[x][y].paint(g, grid[x][y].contains(mousePosition));
            }
        }
    }
}
